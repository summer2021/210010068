/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020. All rights reserved.
 * secGear is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

#include <stdio.h>
#include <unistd.h>
#include <linux/limits.h>
#include "enclave.h"
#include "openssh_u.h"
#include "string.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>

#include <openssl/bn.h>
#include <openssl/dh.h>

#include "misc.h"
#include "sshkey.h"
//#include "bsd-misc.h"
#include "sshbuf.h"
#include "ssherr.h"
#include "atomicio.h"

#define BUF_LEN 32

int main()
{
    int  retval = 0;
    char *path = PATH;
    //char buf[BUF_LEN];
    cc_enclave_t *context = NULL;
    context = (cc_enclave_t *)malloc(sizeof(cc_enclave_t));
    if (!context) {
        return CC_ERROR_OUT_OF_MEMORY;
    }
    cc_enclave_result_t res = CC_FAIL;

    printf("Create secgear enclave\n");

    char real_p[PATH_MAX];
    /* check file exists, if not exist then use absolute path */
    if (realpath(path, real_p) == NULL) {
        if (getcwd(real_p, sizeof(real_p)) == NULL) {
            printf("Cannot find enclave.sign.so");
            goto end;
        }
        if (PATH_MAX - strlen(real_p) <= strlen("/enclave.signed.so")) {
            printf("Failed to strcat enclave.sign.so path");
            goto end;
        }
        (void)strcat(real_p, "/enclave.signed.so");
    }

    res = cc_enclave_create(real_p, AUTO_ENCLAVE_TYPE, 0, SECGEAR_DEBUG_FLAG, NULL, 0, context);
    if (res != CC_SUCCESS) {
        printf("Create enclave error\n");
        goto end; 
    }


    if (res != CC_SUCCESS || retval != (int)CC_SUCCESS) {
        printf("Ecall enclave error\n");
    } else {
        printf("DH generate\n");
    }

    res = cc_enclave_destroy(context);
    if(res != CC_SUCCESS) {
        printf("Destroy enclave error\n");
    }
end:
    free(context);
    return res;
}

/*save public key*/
void
save_public(char *b, size_t b_len, char *path, size_t path_len)
{
	
	int fd, oerrno;
	FILE *f = NULL;
	int r = SSH_ERR_INTERNAL_ERROR;

	if ((fd = open(path, O_WRONLY|O_CREAT|O_TRUNC, 0644)) == -1)
		return;
		// SSH_ERR_SYSTEM_ERROR;
	if ((f = fdopen(fd, "w")) == NULL) {
		r = SSH_ERR_SYSTEM_ERROR;
		goto fail;
	}
	if (fwrite(b, b_len, 1, f) != 1) {
		if (feof(f))
			errno = EPIPE;
		r = SSH_ERR_SYSTEM_ERROR;
		goto out;
	}
	*/
	/* Success */
	r = 0;
	
	//fprintf(f, " %s\n", comment);
	
	if (ferror(f) || fclose(f) != 0) {
		r = SSH_ERR_SYSTEM_ERROR;
 fail:
		oerrno = errno;
		if (f != NULL)
			fclose(f);
		else
			close(fd);
		errno = oerrno;
		//return r;
	}
	
}

/*save private key*/
void
save_private(char *path, size_t path_len, u_char * content, size_t content_len)

{
	
	mode_t omask;

	omask = umask(077);
	
	int fd, oerrno;

	if ((fd = open(path, O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
		return;
		// SSH_ERR_SYSTEM_ERROR;
	if (atomicio(vwrite, fd, content,
	    close(fd)) != 0) {
		oerrno = errno;
		close(fd);
		unlink(path);
		errno = oerrno;
		return;
		// SSH_ERR_SYSTEM_ERROR;
	}
	umask(omask);
	//return r;
	
}

/*sshkey read public key*/
int
sshkey_try_load_public(struct sshkey *kp, size_t kp_len, char *filename, size_t filename_len,
    char *commentp, size_t commentp_len)
{
	FILE *f;
	char *line = NULL, *cp;
	size_t linesize = 0;
	int r;
	struct sshkey *k = NULL;

	kp = NULL;
	commentp = NULL;
	if ((f = fopen(filename, "r")) == NULL)
		return SSH_ERR_SYSTEM_ERROR;
	if ((k = sshkey_new(KEY_UNSPEC)) == NULL) {
		fclose(f);
		return SSH_ERR_ALLOC_FAIL;
	}
	while (getline(&line, &linesize, f) != -1) {
        
		cc_enclave_result_t res = CC_FAIL;
		int retval=0;
		res = unseal_data(&retval, line, linesize);
		if (res != CC_SUCCESS || retval != (int)CC_SUCCESS) {
        	return CC_ERROR_UNEXPECTED;
		} 
        
		cp = line;
		switch (*cp) {
		case '#':
		case '\n':
		case '\0':
			continue;
		}
		/* Abort loading if this looks like a private key */
		if (strncmp(cp, "-----BEGIN", 10) == 0 ||
		    strcmp(cp, "SSH PRIVATE KEY FILE") == 0)
			break;
		/* Skip leading whitespace. */
		for (; *cp && (*cp == ' ' || *cp == '\t'); cp++)
			;
		if (*cp) {
			if ((r = sshkey_read(k, &cp)) == 0) {
				cp[strcspn(cp, "\r\n")] = '\0';
				if (commentp) {
					commentp = strdup(*cp ?
					    cp : filename);
					if (commentp == NULL)
						r = SSH_ERR_ALLOC_FAIL;
				}
				/* success */
				kp = k;
				free(line);
				fclose(f);
				return r;
			}
		}
	}
	free(k);
	free(line);
	fclose(f);
	return SSH_ERR_INVALID_FORMAT;
}


/*sshkey read private key
Load a file from a fd into a buffer */
int
sshbuf_load(int fd, struct sshbuf *blobp, size_t blobp_len)
{
	u_char buf[4096];
	size_t len;
	struct stat st;
	int r;
	struct sshbuf *blob;

	blobp = NULL;

	if (fstat(fd, &st) == -1)
		return SSH_ERR_SYSTEM_ERROR;
	if ((st.st_mode & (S_IFSOCK|S_IFCHR|S_IFIFO)) == 0 &&
	    st.st_size > SSHBUF_SIZE_MAX)
		return SSH_ERR_INVALID_FORMAT;
	if ((blob = sshbuf_new()) == NULL)
		return SSH_ERR_ALLOC_FAIL;
	for (;;) {
		if ((len = atomicio(read, fd, buf, sizeof(buf))) == 0) {
			if (errno == EPIPE)
				break;
			r = SSH_ERR_SYSTEM_ERROR;
			goto out;
		}
		
		/******** prepare to unseal data ***********/
		uint32_t buf_len=strlen((const char *)buf);
        
		cc_enclave_result_t res = CC_FAIL;
		int retval=0;
		res = unseal_data(&retval, buf, sealed_data_len);
		if (res != CC_SUCCESS || retval != (int)CC_SUCCESS) {
        	return CC_ERROR_UNEXPECTED;
		} 
        
		/******** finish unseal data ***********/
		if ((r = sshbuf_put(blob, buf, len)) != 0)
			goto out;
		if (sshbuf_len(blob) > SSHBUF_SIZE_MAX) {
			r = SSH_ERR_INVALID_FORMAT;
			goto out;
		}
	}
	if ((st.st_mode & (S_IFSOCK|S_IFCHR|S_IFIFO)) == 0 &&
	    st.st_size != (off_t)sshbuf_len(blob)) {
		r = SSH_ERR_FILE_CHANGED;
		goto out;
	}
	/* success */
	blobp = blob;
	blob = NULL; /* transferred */
	r = 0;
 out:
	explicit_bzero(buf, sizeof(buf));
	sshbuf_free(blob);
	return r;
}
