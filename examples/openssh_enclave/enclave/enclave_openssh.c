/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020. All rights reserved.
 * secGear is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *     http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdio.h>
#include <string.h>
#include "openssh_enclave_t.h"

#ifndef WITH_OPENSSL
#define WITH_OPENSSL
#define OPENSSL_HAS_ECC
#define WITH_XMSS

#include <sys/types.h>

#ifdef HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif


#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>

#ifdef HAVE_PATHS_H
#include <paths.h>
#endif

#ifdef WITH_OPENSSL
#include <openssl/dh.h>
#include <openssl/bn.h>
#include <openssl/rand.h>
//#include "openbsd-compat/openssl-compat.h"
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/ec.h>
#include <openssl/ecdh.h>
#include <openssl/ecdsa.h>
#include <openssl/objects.h>
#endif

#ifdef HAVE_SECUREWARE
#include <sys/security.h>
#include <prot.h>
#endif

#include "sshkey.h"
#include "servconf.h"
#include "crypto_api.h"
#include "sshbuf.h"
#include "ssherr.h"
#include "kex.h"
#include "sshkey-xmss.h"

#include "status.h"
#include "secgear_dataseal.h"

# define DH		void
# define BIGNUM		void
# define RSA		void
# define DSA		void
# define EC_KEY		void
# define EC_GROUP	void
# define EC_POINT	void

/* Re-exec fds */
#define REEXEC_DEVCRYPTO_RESERVED_FD	(STDERR_FILENO + 1)
#define REEXEC_STARTUP_PIPE_FD		(STDERR_FILENO + 2)
#define REEXEC_CONFIG_PASS_FD		(STDERR_FILENO + 3)
#define REEXEC_MIN_FREE_FD		(STDERR_FILENO + 4)

extern char *__progname;

#define MINIMUM(a, b)	(((a) < (b)) ? (a) : (b))

/* Server configuration options. */
ServerOptions options;

struct {
	struct sshkey	**host_keys;		/* all private host keys */
	struct sshkey	**host_pubkeys;		/* all public host keys */
	struct sshkey	**host_certificates;	/* all public host certificates */
	int		have_ssh2_key;
} sensitive_data;

int
get_hostkey_by_type(int type, int nid, int need_private, struct ssh *ssh, size_t ssh_len, struct sshkey * key, size_t sshkey_len)
{
	u_int i;

	for (i = 0; i < options.num_host_key_files; i++) {
		switch (type) {
		case KEY_RSA_CERT:
		case KEY_DSA_CERT:
		case KEY_ECDSA_CERT:
		case KEY_ED25519_CERT:
		case KEY_ECDSA_SK_CERT:
		case KEY_ED25519_SK_CERT:
		case KEY_XMSS_CERT:
			key = sensitive_data.host_certificates[i];
			break;
		default:
			key = sensitive_data.host_keys[i];
			if (key == NULL && !need_private)
				key = sensitive_data.host_pubkeys[i];
			break;
		}
		if (key == NULL || key->type != type)
			continue;
		switch (type) {
		case KEY_ECDSA:
		case KEY_ECDSA_SK:
		case KEY_ECDSA_CERT:
		case KEY_ECDSA_SK_CERT:
			if (key->ecdsa_nid != nid)
				continue;
			/* FALLTHROUGH */
		default:
			key = need_private ?
			    sensitive_data.host_keys[i] : key;
			return 0;
		}
	}
	key=NULL;
	return 0;
}

static int
dsa_generate_private_key(u_int bits, struct sshkey *k)
{
	DSA **dsap = &k->dsa;
	DSA *private;
	int ret = SSH_ERR_INTERNAL_ERROR;

	if (dsap == NULL)
		return SSH_ERR_INVALID_ARGUMENT;
	if (bits != 1024)
		return SSH_ERR_KEY_LENGTH;
	if ((private = DSA_new()) == NULL) {
		ret = SSH_ERR_ALLOC_FAIL;
		goto out;
	}
	*dsap = NULL;
	if (!DSA_generate_parameters_ex(private, bits, NULL, 0, NULL,
	    NULL, NULL) || !DSA_generate_key(private)) {
		ret = SSH_ERR_LIBCRYPTO_ERROR;
		goto out;
	}
	*dsap = private;
	private = NULL;
	ret = 0;
 out:
	DSA_free(private);
	return ret;
}

int
sshkey_ecdsa_bits_to_nid(int bits)
{
	switch (bits) {
	case 256:
		return NID_X9_62_prime256v1;
	case 384:
		return NID_secp384r1;
# ifdef OPENSSL_HAS_NISTP521
	case 521:
		return NID_secp521r1;
# endif /* OPENSSL_HAS_NISTP521 */
	default:
		return -1;
	}
}

static int
ecdsa_generate_private_key(u_int bits, int *nid, struct sshkey *k)
{
	EC_KEY **ecdsap = &k->ecdsa;
	EC_KEY *private;
	int ret = SSH_ERR_INTERNAL_ERROR;

	if (nid == NULL || ecdsap == NULL)
		return SSH_ERR_INVALID_ARGUMENT;
	if ((*nid = sshkey_ecdsa_bits_to_nid(bits)) == -1)
		return SSH_ERR_KEY_LENGTH;
	*ecdsap = NULL;
	if ((private = EC_KEY_new_by_curve_name(*nid)) == NULL) {
		ret = SSH_ERR_ALLOC_FAIL;
		goto out;
	}
	if (EC_KEY_generate_key(private) != 1) {
		ret = SSH_ERR_LIBCRYPTO_ERROR;
		goto out;
	}
	EC_KEY_set_asn1_flag(private, OPENSSL_EC_NAMED_CURVE);
	*ecdsap = private;
	private = NULL;
	ret = 0;
 out:
	EC_KEY_free(private);
	return ret;
}

static int
rsa_generate_private_key(u_int bits, struct sshkey *k)
{
	RSA **rsap=&k->rsa;
	RSA *private = NULL;
	BIGNUM *f4 = NULL;
	int ret = SSH_ERR_INTERNAL_ERROR;

	if (rsap == NULL)
		return SSH_ERR_INVALID_ARGUMENT;
	if (bits < SSH_RSA_MINIMUM_MODULUS_SIZE ||
	    bits > SSHBUF_MAX_BIGNUM * 8)
		return SSH_ERR_KEY_LENGTH;
	*rsap = NULL;
	if ((private = RSA_new()) == NULL || (f4 = BN_new()) == NULL) {
		ret = SSH_ERR_ALLOC_FAIL;
		goto out;
	}
	if (!BN_set_word(f4, RSA_F4) ||
	    !RSA_generate_key_ex(private, bits, f4, NULL)) {
		ret = SSH_ERR_LIBCRYPTO_ERROR;
		goto out;
	}
	*rsap = private;
	private = NULL;
	ret = 0;
 out:
	RSA_free(private);
	BN_free(f4);
	return ret;
}


/*generate private key*/
int
sshkey_generate(int type, u_int bits, struct sshkey **keyp, size_t keyp_len)
{
	struct sshkey *k;
	int ret = SSH_ERR_INTERNAL_ERROR;

	if (keyp == NULL)
		return SSH_ERR_INVALID_ARGUMENT;
	*keyp = NULL;
	if ((k = sshkey_new(KEY_UNSPEC)) == NULL)
		return SSH_ERR_ALLOC_FAIL;
	switch (type) {
	case KEY_ED25519:
		if ((k->ed25519_pk = malloc(ED25519_PK_SZ)) == NULL ||
		    (k->ed25519_sk = malloc(ED25519_SK_SZ)) == NULL) {
			ret = SSH_ERR_ALLOC_FAIL;
			break;
		}
		crypto_sign_ed25519_keypair(k->ed25519_pk, k->ed25519_sk);
		ret = 0;
		break;
#ifdef WITH_XMSS
	case KEY_XMSS:
		ret = sshkey_xmss_generate_private_key(k, bits);
		break;
#endif /* WITH_XMSS */
#ifdef WITH_OPENSSL
	case KEY_DSA:
		ret = dsa_generate_private_key(bits, k);
		break;
# ifdef OPENSSL_HAS_ECC
	case KEY_ECDSA:
		ret = ecdsa_generate_private_key(bits, &k->ecdsa_nid,
		    k);
		break;
# endif /* OPENSSL_HAS_ECC */
	case KEY_RSA:
		ret = rsa_generate_private_key(bits, k);
		break;
#endif /* WITH_OPENSSL */
	default:
		ret = SSH_ERR_INVALID_ARGUMENT;
	}
	if (ret == 0) {
		k->type = type;
		*keyp = k;
	} else
		sshkey_free(k);
	return ret;
}


uint8_t *malloc_with_check(uint32_t size)
{
    if (size == 0) {
        return NULL;
    }
    uint8_t *ptr = malloc(size);
    return ptr;
}

static int
sshkey_save_private_blob(struct sshbuf *keybuf, char *filename)
{
	cc_enclave_result_t ret;
	u_char * seal_data=sshbuf_mutable_ptr(keybuf);

	/******** prepare to seal data *********/
	uint32_t data_len = strlen((const char *)seal_data);
	uint8_t additional_text[] = "private key";
    uint32_t add_len = strlen((const char *)additional_text);
	
    size_t sealed_data_len = cc_enclave_get_sealed_data_size(data_len, add_len);
    if (sealed_data_len == UINT32_MAX)
        return CC_ERROR_OUT_OF_MEMORY;
    
    cc_enclave_sealed_data_t *sealed_data = (cc_enclave_sealed_data_t *)malloc_with_check(sealed_data_len);
    if (sealed_data == NULL) 
        return CC_ERROR_OUT_OF_MEMORY;

    ret = cc_enclave_seal_data(seal_data, data_len, sealed_data, sealed_data_len, additional_text, add_len);
	uint32_t buf_len=sizeof(cc_enclave_sealed_data_t);
	u_char *buf=malloc_with_check(buf_len);
	memcpy(buf, sealed_data, buf_len);
	/******** finish seal data *********/

    if (ret != CC_SUCCESS) goto error3;
	
	//cc_enclave_result_t res = CC_FAIL;
	//int retval=0;
	size_t filename_len=strlen((const char*)filename);
	save_private(filename, filename_len, buf, sealed_data_len);
	//if (res != CC_SUCCESS || retval != (int)CC_SUCCESS)
	//	return CC_ERROR_UNEXPECTED;

	error3:
		free(sealed_data);
		return ret;
}

/* Save a private key */
int
sshkey_save_private(struct sshkey *key, size_t sshkey_len, char *filename, size_t filename_len,
    char *passphrase, size_t passphrase_len, char *comment, size_t comment_len, int format, 
	char *openssh_format_cipher,  size_t openssh_len, int openssh_format_rounds)
{
	struct sshbuf *keyblob = NULL;
	int r;

	if ((keyblob = sshbuf_new()) == NULL)
		return SSH_ERR_ALLOC_FAIL;
	if ((r = sshkey_private_to_fileblob(key, keyblob, passphrase, comment,
	    format, openssh_format_cipher, openssh_format_rounds)) != 0)
		goto out;
	if ((r = sshkey_save_private_blob(keyblob, filename)) != 0)
		goto out;
	r = 0;
	out:
		sshbuf_free(keyblob);
		return r;
}

/*generate public key*/
int
sshkey_from_private(struct sshkey *k, size_t k_len, struct sshkey **pkp, size_t pkp_len)
{
	struct sshkey *n = NULL;
	int r = SSH_ERR_INTERNAL_ERROR;
#ifdef WITH_OPENSSL
	const BIGNUM *rsa_n, *rsa_e;
	BIGNUM *rsa_n_dup = NULL, *rsa_e_dup = NULL;
	const BIGNUM *dsa_p, *dsa_q, *dsa_g, *dsa_pub_key;
	BIGNUM *dsa_p_dup = NULL, *dsa_q_dup = NULL, *dsa_g_dup = NULL;
	BIGNUM *dsa_pub_key_dup = NULL;
#endif /* WITH_OPENSSL */

	*pkp = NULL;
	if ((n = sshkey_new(k->type)) == NULL) {
		r = SSH_ERR_ALLOC_FAIL;
		goto out;
	}
	switch (k->type) {
#ifdef WITH_OPENSSL
	case KEY_DSA:
	case KEY_DSA_CERT:
		DSA_get0_pqg(k->dsa, &dsa_p, &dsa_q, &dsa_g);
		DSA_get0_key(k->dsa, &dsa_pub_key, NULL);
		if ((dsa_p_dup = BN_dup(dsa_p)) == NULL ||
		    (dsa_q_dup = BN_dup(dsa_q)) == NULL ||
		    (dsa_g_dup = BN_dup(dsa_g)) == NULL ||
		    (dsa_pub_key_dup = BN_dup(dsa_pub_key)) == NULL) {
			r = SSH_ERR_ALLOC_FAIL;
			goto out;
		}
		if (!DSA_set0_pqg(n->dsa, dsa_p_dup, dsa_q_dup, dsa_g_dup)) {
			r = SSH_ERR_LIBCRYPTO_ERROR;
			goto out;
		}
		dsa_p_dup = dsa_q_dup = dsa_g_dup = NULL; /* transferred */
		if (!DSA_set0_key(n->dsa, dsa_pub_key_dup, NULL)) {
			r = SSH_ERR_LIBCRYPTO_ERROR;
			goto out;
		}
		dsa_pub_key_dup = NULL; /* transferred */

		break;
# ifdef OPENSSL_HAS_ECC
	case KEY_ECDSA:
	case KEY_ECDSA_CERT:
	case KEY_ECDSA_SK:
	case KEY_ECDSA_SK_CERT:
		n->ecdsa_nid = k->ecdsa_nid;
		n->ecdsa = EC_KEY_new_by_curve_name(k->ecdsa_nid);
		if (n->ecdsa == NULL) {
			r = SSH_ERR_ALLOC_FAIL;
			goto out;
		}
		if (EC_KEY_set_public_key(n->ecdsa,
		    EC_KEY_get0_public_key(k->ecdsa)) != 1) {
			r = SSH_ERR_LIBCRYPTO_ERROR;
			goto out;
		}
		if (k->type != KEY_ECDSA_SK && k->type != KEY_ECDSA_SK_CERT)
			break;
		/* Append security-key application string */
		size_t lenapp=strlen(k->sk_application);
		if ((n->sk_application = strndup(k->sk_application, lenapp)) == NULL)
			goto out;
		break;
# endif /* OPENSSL_HAS_ECC */
	case KEY_RSA:
	case KEY_RSA_CERT:
		RSA_get0_key(k->rsa, &rsa_n, &rsa_e, NULL);
		if ((rsa_n_dup = BN_dup(rsa_n)) == NULL ||
		    (rsa_e_dup = BN_dup(rsa_e)) == NULL) {
			r = SSH_ERR_ALLOC_FAIL;
			goto out;
		}
		if (!RSA_set0_key(n->rsa, rsa_n_dup, rsa_e_dup, NULL)) {
			r = SSH_ERR_LIBCRYPTO_ERROR;
			goto out;
		}
		rsa_n_dup = rsa_e_dup = NULL; /* transferred */
		break;
#endif /* WITH_OPENSSL */
	case KEY_ED25519:
	case KEY_ED25519_CERT:
	case KEY_ED25519_SK:
	case KEY_ED25519_SK_CERT:
		if (k->ed25519_pk != NULL) {
			if ((n->ed25519_pk = malloc(ED25519_PK_SZ)) == NULL) {
				r = SSH_ERR_ALLOC_FAIL;
				goto out;
			}
			memcpy(n->ed25519_pk, k->ed25519_pk, ED25519_PK_SZ);
		}
		if (k->type != KEY_ED25519_SK &&
		    k->type != KEY_ED25519_SK_CERT)
			break;
		/* Append security-key application string */
		//size_t
		lenapp=strlen(k->sk_application);
		if ((n->sk_application = strndup(k->sk_application, lenapp)) == NULL)
			goto out;
		break;
#ifdef WITH_XMSS
	case KEY_XMSS:
	case KEY_XMSS_CERT:
		if ((r = sshkey_xmss_init(n, k->xmss_name)) != 0)
			goto out;
		if (k->xmss_pk != NULL) {
			u_int32_t left;
			size_t pklen = sshkey_xmss_pklen(k);
			if (pklen == 0 || sshkey_xmss_pklen(n) != pklen) {
				r = SSH_ERR_INTERNAL_ERROR;
				goto out;
			}
			if ((n->xmss_pk = malloc(pklen)) == NULL) {
				r = SSH_ERR_ALLOC_FAIL;
				goto out;
			}
			memcpy(n->xmss_pk, k->xmss_pk, pklen);
			/* simulate number of signatures left on pubkey */
			left = sshkey_xmss_signatures_left(k);
			if (left)
				sshkey_xmss_enable_maxsign(n, left);
		}
		break;
#endif /* WITH_XMSS */
	default:
		r = SSH_ERR_KEY_TYPE_UNKNOWN;
		goto out;
	}
	if (sshkey_is_cert(k) && (r = sshkey_cert_copy(k, n)) != 0)
		goto out;
	/* success */
	*pkp = n;
	n = NULL;
	r = 0;
 out:
	sshkey_free(n);
#ifdef WITH_OPENSSL
	BN_clear_free(rsa_n_dup);
	BN_clear_free(rsa_e_dup);
	BN_clear_free(dsa_p_dup);
	BN_clear_free(dsa_q_dup);
	BN_clear_free(dsa_g_dup);
	BN_clear_free(dsa_pub_key_dup);
#endif

	return r;
}

/* Save a public key */
int
sshkey_save_public(struct sshkey *key, size_t sshkey_len, char *path, size_t path_len,
    char *comment, size_t comment_len)
{
	
	//if ((r = sshkey_write(key, f)) != 0)
		//goto fail;
	struct sshbuf *b = NULL;
	int r = SSH_ERR_INTERNAL_ERROR;

	if ((b = sshbuf_new()) == NULL)
		return SSH_ERR_ALLOC_FAIL;
	if ((r = sshkey_format_text(key, b)) != 0){
		sshbuf_free(b);
		return r;
	}
		
	
	/******** prepare to seal data *********/	
	uint32_t data_len = sshbuf_len(b);
	u_char *seal_data=malloc_with_check(data_len);
	memcpy(seal_data, sshbuf_ptr(b), data_len);
	
	uint8_t additional_text[] = "public key";
    uint32_t add_len = strlen((const char *)additional_text);
	cc_enclave_result_t ret;
	
    uint32_t sealed_data_len = cc_enclave_get_sealed_data_size(data_len, add_len);
    if (sealed_data_len == UINT32_MAX)
        return CC_ERROR_OUT_OF_MEMORY;
    
    cc_enclave_sealed_data_t *sealed_data = (cc_enclave_sealed_data_t *)malloc_with_check(sealed_data_len);
    if (sealed_data == NULL) 
        return CC_ERROR_OUT_OF_MEMORY;
	
	
    ret = cc_enclave_seal_data(seal_data, data_len, sealed_data, sealed_data_len, additional_text, add_len);
	
	size_t buf_len=sizeof(cc_enclave_sealed_data_t);
	char *buf=(char *)malloc(buf_len);
	memcpy(buf, sealed_data, buf_len);
	/******** finish seal data *********/

	//cc_enclave_result_t res = CC_FAIL;
	//int retval=0;
	save_public(buf, buf_len, path, path_len);
	//if (res != CC_SUCCESS || retval != (int)CC_SUCCESS)
	//	goto out;

	return ret;
}

/*unseal key*/
int unseal_data(u_char *buf, size_t buf_len)
{
	/*transfer u_char to cc_enclave_sealed_data_t*/
	cc_enclave_sealed_data_t *sealed_data=(cc_enclave_sealed_data_t *)malloc_with_check(buf_len);
	sealed_data=(cc_enclave_sealed_data_t *)buf;

	cc_enclave_result_t ret;
	uint32_t encrypt_add_len = cc_enclave_get_add_text_size(sealed_data);
	uint32_t encrypt_data_len = cc_enclave_get_encrypted_text_size(sealed_data);
	uint8_t *decrypted_seal_data = malloc_with_check(encrypt_data_len);
	//char buftmp[encrypt_data_len];

	if (decrypted_seal_data == NULL) {
		ret = CC_ERROR_OUT_OF_MEMORY;
		goto error3;
	}
	uint8_t *demac_data = malloc_with_check(encrypt_add_len);
	if (demac_data == NULL) {
		ret = CC_ERROR_OUT_OF_MEMORY;
		goto error2;
	}
	ret = cc_enclave_unseal_data(sealed_data,
	decrypted_seal_data, &encrypt_data_len, demac_data, &encrypt_add_len);
	if (ret != CC_SUCCESS) {
		goto error1;
	}
	
	memcpy(buf, decrypted_seal_data, encrypt_data_len);

error1:
	free(demac_data);
error2:
	free(decrypted_seal_data);
error3:
	free(sealed_data);

	return ret;
}

#endif /* WITH_OPENSSL */